#include <jni.h>
#include <errno.h>
#include <android/sensor.h>
#include <android/log.h>
#include <vulkan/vulkan_core.h>
#include <vulkan/vulkan.h>
#include <vulkan/vulkan_android.h>
#include <android_native_app_glue.h>
#include <android/native_window.h>
#include "main.h"
#include "vulkan-framework/include/Instance.h"

using namespace NeroReflex;

#define LOGI(...) ((void)__android_log_print(ANDROID_LOG_INFO, "native-activity", __VA_ARGS__))
#define LOGW(...) ((void)__android_log_print(ANDROID_LOG_WARN, "native-activity", __VA_ARGS__))
#define LOGE(...) ((void)__android_log_print(ANDROID_LOG_ERROR, "native-activity", __VA_ARGS__))

/**
 * Our saved state data.
 */
struct saved_state {
    float angle;
    int32_t x;
    int32_t y;
};

/**
 * Shared state for our app.
 */
struct engine {
private:
    static struct engine* self;

public:
    static struct engine* getInstance() {
        return self;
    }

    struct android_app* app;

    std::unique_ptr<VulkanFramework::Instance> instance;

    VkSurfaceKHR surface;

    VulkanFramework::Device* device;

    ASensorManager* sensorManager;

    bool animating;

    int32_t width;
    int32_t height;

    struct saved_state state;

};

struct engine* engine::self = new engine();

/**
 * Initialize a vulkan context for the current display.
 */
static int engine_init_display(struct engine* engine, ANativeWindow* window) {
    VkAndroidSurfaceCreateInfoKHR surfaceCreateInfo = {};
    surfaceCreateInfo.sType = VK_STRUCTURE_TYPE_ANDROID_SURFACE_CREATE_INFO_KHR;
    surfaceCreateInfo.pNext = nullptr;
    surfaceCreateInfo.flags = 0; // flags is reserved for future use
    surfaceCreateInfo.window = window;

    if (surfaceCreateInfo.window == nullptr) {
        LOGE("bad ANativeWindow*");
        abort();
    }

    VK_CHECK_RESULT(vkCreateAndroidSurfaceKHR(engine->instance->getNativeInstanceHandle(), &surfaceCreateInfo, nullptr, &engine->surface));

    engine->device = engine->instance->openDevice(
            {
                    VulkanFramework::QueueFamily::ConcreteQueueFamilyDescriptor(
                            {
                                    VulkanFramework::QueueFamily::QueueFamilySupportedOperationType::Transfer,
                                    VulkanFramework::QueueFamily::QueueFamilySupportedOperationType::Compute,
                                    VulkanFramework::QueueFamily::QueueFamilySupportedOperationType::Graphics,
                                    VulkanFramework::QueueFamily::QueueFamilySupportedOperationType::Present
                            },
                            1
                    )
            },
            std::vector<std::string>({}),
            engine->surface,
            std::function<VkBool32(VkInstance, VkPhysicalDevice, uint32_t)>(
                    [&engine](VkInstance instance, VkPhysicalDevice phisicalDevice, uint32_t queueFamilyIndex) -> VkBool32 {
                        VkBool32 res;
                        vkGetPhysicalDeviceSurfaceSupportKHR(phisicalDevice, queueFamilyIndex, engine->surface, &res);

                        return res;
                    })
                    );

    return 0;
}

/**
 * Just the current frame in the display.
 */
static void engine_draw_frame(struct engine* engine) {
    if (engine->surface == VK_NULL_HANDLE) {

        LOGI("frame rendered.");

        // no vulkan surface
        return;
    }
}

/**
 * Tear down the vulkan context currently associated with the display.
 */
static void engine_term_display(struct engine* engine) {
    if (engine->surface != VK_NULL_HANDLE) {
        // TODO: spegni tutto.


        engine->surface = VK_NULL_HANDLE;
    }

    engine->animating = 0;
}

extern "C" {

/**
 * Process the next input event.
 */
static int32_t engine_handle_input(struct android_app *app, AInputEvent *event) {
    struct engine *engine = engine::getInstance();
    /*if (AInputEvent_getType(event) == AINPUT_EVENT_TYPE_MOTION) {
        engine->animating = 1;
        engine->state.x = AMotionEvent_getX(event, 0);
        engine->state.y = AMotionEvent_getY(event, 0);
        return 1;
    }*/

    return 0;
}

/**
 * Process the next main command.
 */
static void engine_handle_cmd(struct android_app *app, int32_t cmd) {
    struct engine *engine = engine::getInstance();

    switch (cmd) {
        case APP_CMD_SAVE_STATE:
            // The system has asked us to save our current state.  Do so.
            engine->app->savedState = malloc(sizeof(struct saved_state));
            *((struct saved_state *) engine->app->savedState) = engine->state;
            engine->app->savedStateSize = sizeof(struct saved_state);
            break;
        case APP_CMD_INIT_WINDOW:
            // The window is being shown, get it ready.
            if (app->window != nullptr) {
                engine_init_display(engine, app->window);
                engine_draw_frame(engine);
            }
            break;
        case APP_CMD_TERM_WINDOW:
            // The window is being hidden or closed, clean it up.
            engine_term_display(engine);
            break;
        case APP_CMD_GAINED_FOCUS:
            // TODO: When our app gains focus, we start monitoring the accelerometer.

            // Nothing graphics-related to do here

            break;
        case APP_CMD_LOST_FOCUS:
            // TODO: When our app loses focus, we stop monitoring the accelerometer.

            // Also stop animating.
            engine->animating = false;

            engine_draw_frame(engine);

            break;
    }
}

/**
 * This is the main entry point of a native application that is using
 * android_native_app_glue.  It runs in its own thread, with its own
 * event loop for receiving input events and doing other things.
 */
void android_main(struct android_app *state) {

    // Make sure glue is not stripped.
    app_dummy();

    struct engine *engine = engine::getInstance();

    engine->app = state;
    engine->animating = false;
    engine->surface = VK_NULL_HANDLE;
    engine->instance = std::move(std::make_unique<VulkanFramework::Instance>(
            std::vector<std::string>({
                std::string("VK_KHR_surface"),
                std::string("VK_KHR_android_surface")
            })
            ));

    state->userData = reinterpret_cast<void *>(&engine);
    state->onAppCmd = engine_handle_cmd;
    state->onInputEvent = engine_handle_input;

    // loop waiting for stuff to do.

    while (true) {
        // Read all pending events.
        int ident;
        int events;
        struct android_poll_source *source;

        // If not animating, we will block forever waiting for events.
        // If animating, we loop until all events are read, then continue
        // to draw the next frame of animation.
        while ((ident = ALooper_pollAll(engine->animating ? 0 : -1, NULL, &events,
                                        (void **) &source)) >= 0) {

            // Process this event.
            if (source != NULL) {
                source->process(state, source);
            }

            // If a sensor has data, process it now.
            if (ident == LOOPER_ID_USER) {
                /*if (engine.accelerometerSensor != NULL) {
                    ASensorEvent event;
                    while (ASensorEventQueue_getEvents(engine.sensorEventQueue,
                                                       &event, 1) > 0) {
                        LOGI("accelerometer: x=%f y=%f z=%f",
                             event.acceleration.x, event.acceleration.y,
                             event.acceleration.z);
                    }
                }*/
            }

            // Check if we are exiting.
            if (state->destroyRequested != 0) {
                engine_term_display(engine);
                return;
            }

        }

        // Done with events; draw next animation frame.
        if (engine->animating) {


            // Drawing is throttled to the screen update rate, so there
            // is no need to do timing here.
            engine_draw_frame(engine);
        }
    }
}

} // extern "C"